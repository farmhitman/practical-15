#include <iostream>

const int N = 10;


void FindOddNumbers(int mLimit, bool mIsOdd) {
	for (int i = 0; i < mLimit; i++) {
		if ((i % 2) == mIsOdd) {
			printf("%s number is %d\n", mIsOdd ? "Odd" : "Even", i);
		}
	}
}

int main()
{
    for (int i = 0; i < N; i++) {
        if (!(i % 2)) {
            printf("Even number is %d\n", i);
        }
    }
    FindOddNumbers(10, true);
}